/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//Variables Declared
var placeSearch, autocomplete;

//Components of Address
var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
};
var markers = [];
var map;
var marker;
var marker1;

//Start of initialize routine
function initialize() {
    var mapOptions = {
        center: new google.maps.LatLng(36.73888412439431, 23.203125),
        zoom: 3
    };
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    autocomplete = new google.maps.places.Autocomplete((document.getElementById('autocomplete')), {types: ['geocode']});
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
// Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        for (var component in componentForm) {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
        }
        var newPos = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
        map.setOptions({
            center: newPos,
            zoom: 15
        });

//Add a marker
        marker1 = new google.maps.Marker({
            position: newPos,
            map: map,
            title: "New marker"
        });
// Get each component of the address from the place details and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }
        document.getElementById("la").value = newPos;
    });
    google.maps.event.addListener(map, 'click', function(event) {
        placeMarker(event.latLng);
        document.getElementById("autocomplete").value = "";
        lat = event.latLng.lat();
        lng = event.latLng.lng();
        var latlng = new google.maps.LatLng(lat, lng);
        document.getElementById("la").value = latlng;
// Create a geocoder object
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'latLng': latlng}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK)
            {
// If address found, pass to processing function
                for (var i = 0; i < results[0].address_components.length; i++)
                {
                    var addr = results[0].address_components[i];
                    if (addr.types[0] === 'country')
                    {
                        document.getElementById("country").value = addr.long_name;
                    }
                    if (addr.types[0] === 'locality')
                    {
                        document.getElementById("locality").value = addr.long_name;
                    }
                    if (addr.types[0] === 'route')
                    {
                        document.getElementById("route").value = addr.long_name;
                    }
                    if (addr.types[0] === 'street_number')
                    {
                        document.getElementById("street_number").value = addr.long_name;
                    }
                    if (addr.types[0] === 'administrative_area_level_1')
                    {
                        document.getElementById("administrative_area_level_1").value = addr.long_name;
                    }
                    if (addr.types[0] === 'postal_code')
                    {
                        document.getElementById("postal_code").value = addr.long_name;
                    }
                }

            }
        });

    });
}
google.maps.event.addDomListener(window, 'load', initialize);
function placeMarker(latlng) {
    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        title: "marker"
    });
    ClearMarkers();
    markers.push(marker);
}
function ClearMarkers(map) {

    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
    markers = [];

}
function CleanMarker1()
{
    marker1.setMap(null);
}
 